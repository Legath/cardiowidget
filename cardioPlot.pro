#-------------------------------------------------
#
# Project created by QtCreator 2014-08-04T14:31:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cardioPlot
TEMPLATE = app


SOURCES += main.cpp\
        cardioplot.cpp

HEADERS  += cardioplot.h
