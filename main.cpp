#include "cardioplot.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CardioPlot w;
    w.show();

    return a.exec();
}
