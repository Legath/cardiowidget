#include "cardioplot.h"

CardioPlot::CardioPlot(QWidget *parent)
    : QWidget(parent)
{
    enable3D = true;
    antialias = true;
    debugTimer = new QTimer(this);
    debugTimer->setInterval(10);
    connect(debugTimer,SIGNAL(timeout()),this,SLOT(timeChanged()));
    debugTimer->start();
    interval=this->width()/100;
    x=0;
    y=this->height()*2/3;
}

CardioPlot::~CardioPlot()
{

}
void CardioPlot::timeChanged()
{
    interval=this->width()/100;
    if(x>=this->width())
        x=0;
    x= x+interval;
    update();
}
void CardioPlot::setEnable3D(bool enable)
{
    enable3D = enable;
}
void CardioPlot::setAntialias(bool enable)
{
    antialias = enable;
}
void CardioPlot::paintEvent(QPaintEvent * e)
{
    Q_UNUSED(e);
    QPainter p(this);
    if(antialias)
        p.setRenderHint(QPainter::Antialiasing, true);
    QLinearGradient grad1(0, 0, 0, this->height());
    grad1.setColorAt(0.0,QColor("#696969"));
    grad1.setColorAt(1.0, QColor("#191A19"));
    p.fillRect(0,0,this->width(),this->height(),grad1);
    //рисуем точку
    QPen dotPen(Qt::green);
    dotPen.setCapStyle(Qt::RoundCap);
    dotPen.setWidth(20);
    p.setPen(dotPen);
    p.drawPoint(x,y);


    //Рисуем рамку

    char frameLineWidth = 20;
    QPen linesPen;
    linesPen.setColor(QColor("#D4D4D4"));
    linesPen.setWidth(frameLineWidth);
    p.setPen(linesPen);
    p.drawLine(0,0,0,this->height());
    p.drawLine(0,this->height(),this->width(),this->height());
    p.drawLine(0,0,this->width(),0);
    p.drawLine(this->width(),0,this->width(),this->height());

    //pseudo 3d
    if(enable3D)
    {
        QPen threeDPen;
        threeDPen.setColor(Qt::black);
        threeDPen.setWidth(2);
        p.setPen(threeDPen);

        p.drawLine(0,0,frameLineWidth/2,frameLineWidth/2);
        p.drawLine(0,this->height(),frameLineWidth/2,this->height() - (frameLineWidth/2));
        p.drawLine(this->width(),0, this->width() - (frameLineWidth/2),frameLineWidth/2);
        p.drawLine(this->width(),this->height(), this->width() - (frameLineWidth/2),this->height() - frameLineWidth/2);
        //outer rim
        p.drawLine(0,0,0,this->height());
        p.drawLine(0,this->height(),this->width(),this->height());
        p.drawLine(0,0,this->width(),0);
        p.drawLine(this->width(),0,this->width(),this->height());
        //inner rim
        p.drawLine(frameLineWidth/2,frameLineWidth/2,frameLineWidth/2,this->height() - frameLineWidth/2);
        p.drawLine(frameLineWidth/2,frameLineWidth/2,this->width() - frameLineWidth/2,frameLineWidth/2);
        p.drawLine(this->width() - frameLineWidth/2, frameLineWidth/2 , this->width() - frameLineWidth/2 , this->height() - frameLineWidth/2);
        p.drawLine(frameLineWidth/2, this->height() - frameLineWidth/2, this->width()-frameLineWidth/2, this->height() - frameLineWidth/2);
    }

    //Рисуем координатную сетку
    linesPen.setWidth(1);
    linesPen.setStyle(Qt::DotLine);
    p.setPen(linesPen);
    int levelWidth = 0;
    levelWidth = this->height()/(numbersOfVerticalLevels+1);
    for (int i = 0 ;i <= numbersOfVerticalLevels;i++)
    {
        p.drawLine(0,levelWidth*(i+1),this->width(),levelWidth*(i+1));
    }
    levelWidth = this->width()/(numbersOfHorizontalLevels+1);
    for (int i = 0 ;i <= numbersOfHorizontalLevels ;i++)
    {
        p.drawLine(levelWidth*(i+1),0,levelWidth*(i+1),this->height());
    }

}
