#ifndef CARDIOPLOT_H
#define CARDIOPLOT_H

#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <QDebug>
class CardioPlot : public QWidget
{
    Q_OBJECT

public:
    CardioPlot(QWidget *parent = 0);
    ~CardioPlot();
    void setEnable3D(bool enable);
    void setAntialias(bool enable);
protected:
    virtual void paintEvent(QPaintEvent * e);
private:
    char numbersOfHorizontalLevels = 16;
    char numbersOfVerticalLevels= 20;
    bool enable3D;
    bool antialias;
    QTimer *debugTimer;
    int x,y,interval;

private slots:
    void timeChanged();
};

#endif // CARDIOPLOT_H
